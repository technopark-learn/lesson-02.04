import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('register')
  registerNewUser(@Query() params) {
    return this.appService.registerNewUser(params);
  }

  @Get('get-user')
  getGetData(@Query() params) {
    return this.appService.getUser(params);
  }
}