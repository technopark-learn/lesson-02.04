import { Injectable, Global } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '../entities/user.entity';

@Global()
@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async find(name, password) {
    return this.userRepository.findOne({
      where: {
        name,
        password
      },
    });
  }

  async create(name, password) {
    const existsUser = await this.find(name, password);
    if (existsUser) {
      throw new Error('Такой пользователь уже существует');
    }

    const user = this.userRepository.create();

    user.name = name
    user.password = password
    user.createdAt = new Date();

    await this.userRepository.save(user);

    return user;
  }
}
