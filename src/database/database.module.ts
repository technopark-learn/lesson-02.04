import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';
import { UserService } from './services/user.service';

const entities = [UserEntity];

@Global()
@Module({
    imports: [
        TypeOrmModule.forRootAsync({
          inject: [],
          useFactory: () => {
    
            return {
              type: 'postgres',
              host: 'localhost',
              port: 5432,
              username: 'test_user',
              password: 'test_password',
              database: 'test_db',
              entities: entities,
              synchronize: true,
            };
          },
        }),
        TypeOrmModule.forFeature(entities),
      ],
    providers: [UserService],
    exports: [UserService]
})
export class DatabaseModule {}