import { Inject, Injectable } from '@nestjs/common';
import { UserService } from './database/services/user.service';

@Injectable()
export class AppService {

  constructor(private userService: UserService) {
    
  }

  registerNewUser(params) {
    const user = params.user;
    const password = params.password;

    if (!user) {
      return 'Error; user field not found';
    }

    if (!password) {
      return 'Error; password field not found';
    }

    return this.userService.create(user, password);
  }

  getUser(params) {
    const user = params.user;
    const password = params.password;

    return this.userService.find(user, password);
  }
}
